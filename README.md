# life-game
The Conway's Game of Life(famous simulator of artificial life ) with GPU acceleration

## Dependency
* Python(==3.8)
* NVIDIA Driver(==460.32.03)
* CUDA(==11.2)
* VSCode(>=1.55.1)

## Setup
1. Create python virtual environment
    ```bash
    python3.8 -m venv venv
    ```

2. Install requirement packages
   ```bash
   pip install -r requirements.txt
   ```

## Usage
1. Execute application(**I recommend do it in the VSCode integrated terminal**)
   ```bash
   python LifeGameApplication.py
   ```

## Author
abacus

## Attributions
* Icons used in the application belong to [LineIcons](https://lineicons.com/)
