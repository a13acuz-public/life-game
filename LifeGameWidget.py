'''
Copyright (c) 2021 abacus
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses/.
'''

import numpy as np
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *


class ImageWidget(QWidget):

    def __init__(self, Parent, WidgetSize=800):
        super().__init__(Parent)
        self._Parent = Parent
        self._WidgetSize = WidgetSize
        self.InitWidget()
        self._MakeColorTable()
        self._FixWidgetSize()

    def _InitMouse(self):
        self._Position = None

    def InitWidget(self):
        self._InitMouse()
        self._Image = None
        self._Region = None
        self._Size = None

        self.update()

    def _InitRegion(self):
        self._Region = QRect(QPoint(0, 0), QSize(1, 1))
        Position = QPoint(int(self._Size.width() / 2),
                          int(self._Size.height() / 2))
        self._SetRegion(Position=Position, Size=self._Size)

    def _MakeColorTable(self):
        self._ColorTable = [qRgb(int(i / 4), i, int(i / 2))
                            for i in range(256)]

    def _FixWidgetSize(self):
        self.setFixedSize(self._WidgetSize, self._WidgetSize)
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

    def _Matrix2QPixmap(self, Matrix):
        NewMatrix = np.require(Matrix * 255, np.uint8, "C")
        Image = QImage(NewMatrix.data, NewMatrix.shape[1], NewMatrix.shape[0], NewMatrix.shape[1],
                       QImage.Format_Indexed8)
        Image.setColorTable(self._ColorTable)
        Pixmap = QPixmap.fromImage(Image)

        return Pixmap

    def _DrawImage(self):
        Painter = QPainter(self)
        Image = self._Image.copy(self._Region)
        Painter.drawPixmap(0, 0, self._WidgetSize, self._WidgetSize, Image)

    def DrawMatrix(self, Matrix):
        self._Image = self._Matrix2QPixmap(Matrix)

        if self._Size is None:
            self._Size = self._Image.size()
            self._InitRegion()

        self.update()

    def _DrawRectangle(self, Rect, Pen=None, Brush=None):
        Painter = QPainter(self)
        if Pen is not None:
            Painter.setPen(Pen)

        if Brush is not None:
            Painter.setBrush(Brush)

        Painter.drawRect(Rect)

    def _DrawGrid(self):
        CellSize = self._WidgetSize / self._Region.width()
        LineWidth = int(CellSize * 0.1)
        if not LineWidth:
            return

        Painter = QPainter(self)
        Pen = QPen(Qt.gray, LineWidth)
        Painter.setPen(Pen)

        for i in range(self._Region.width() + 1):
            Start = QPointF(CellSize * i, 0)
            End = QPointF(CellSize * i, self._WidgetSize)
            Painter.drawLine(Start, End)

        for i in range(self._Region.height() + 1):
            Start = QPointF(0, CellSize * i)
            End = QPointF(self._WidgetSize, CellSize * i)
            Painter.drawLine(Start, End)

    def _AdjustSize(self, Size):

        if Size.width() < 3:
            Size = QSize(3, 3)

        elif (self._Size.width() <= self._WidgetSize) and (Size.width() > self._Size.width()):
            Size = self._Size

        elif (self._Size.width() > self._WidgetSize) and (Size.width() > self._WidgetSize):
            Size = QSize(self._WidgetSize, self._WidgetSize)

        return Size

    def _AdjustPosition(self, Region):

        if Region.top() < 0:
            Region.moveTop(0)

        if Region.bottom() > self._Size.height() - 1:
            Region.moveBottom(self._Size.height() - 1)

        if Region.left() < 0:
            Region.moveLeft(0)

        if Region.right() > self._Size.width() - 1:
            Region.moveRight(self._Size.width() - 1)

        return Region

    def _SetRegion(self, Position=None, Size=None):

        if Size is not None:
            Center = self._Region.center()
            Size = self._AdjustSize(Size)
            self._Region.setSize(Size)
            self._Region.moveCenter(Center)

        if Position is not None:
            self._Region.moveCenter(Position)

        self._Region = self._AdjustPosition(self._Region)

    def ZoomIn(self):

        if self._Image is None:
            return

        Size = QSize(int(self._Region.width() * 0.5),
                     int(self._Region.height() * 0.5))
        self._SetRegion(Size=Size)
        self.update()

    def ZoomOut(self):

        if self._Image is None:
            return

        Size = QSize(int(self._Region.width() * 1.5),
                     int(self._Region.height() * 1.5))
        self._SetRegion(Size=Size)
        self.update()

    def ResetZoom(self):

        if self._Image is None:
            return

        self._InitRegion()
        self.update()

    def mousePressEvent(self, event):

        if self._Image is None:
            return

        if (event.buttons() == Qt.LeftButton) or (event.buttons() == Qt.RightButton):
            self._Position = event.pos()

    def mouseMoveEvent(self, event):

        if self._Image is None:
            return

        if (event.buttons() == Qt.LeftButton) and (self._Position is not None):
            Start = np.array([self._Position.x(), self._Position.y()])
            End = np.array([event.pos().x(), event.pos().y()])

            if (np.linalg.norm(End - Start) >= 5):
                self._InitMouse()

        elif event.buttons() == Qt.RightButton:

            dx = int((event.pos().x() - self._Position.x()) *
                     self._Size.width() / (self._WidgetSize * 10))
            dy = int((event.pos().y() - self._Position.y()) *
                     self._Size.height() / (self._WidgetSize * 10))

            Position = QPoint(self._Region.center().x() - dx,
                              self._Region.center().y() - dy)
            self._SetRegion(Position=Position)
            self.update()

    def mouseReleaseEvent(self, event):

        if self._Image is None:
            return

        if (event.button() == Qt.LeftButton) and (self._Position is not None):

            x = int(self._Region.x() + self._Position.x() *
                    self._Region.width() / self._WidgetSize)
            y = int(self._Region.y() + self._Position.y() *
                    self._Region.height() / self._WidgetSize)

            self._Parent.Flip(x, y)

        self._InitMouse()

    def paintEvent(self, event):

        if self._Image is None:
            self._DrawRectangle(QRect(QPoint(0, 0), QSize(
                self._WidgetSize, self._WidgetSize)), Brush=QBrush(Qt.black))
            return

        self._DrawImage()
        self._DrawGrid()
