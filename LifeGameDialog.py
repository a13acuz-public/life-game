'''
Copyright (c) 2021 abacus
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses/.
'''

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *


class AbstractLifeGameDialog(QDialog):

    def __init__(self, Parent=None):
        super().__init__(Parent)

        self._MakeMainLayout()
        self._SetSignal()

    def _FixWindowSize(self, Width, Height):
        self.setFixedSize(Width, Height)
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

    def _MakeGPUCheckBox(self):
        self._GPUCheckBox = QCheckBox("GPU", self)
        self._GPUCheckBox.setCheckState(Qt.CheckState(2))
        self._MainLayout.addWidget(self._GPUCheckBox)

    def _MakeButtons(self):
        self._Buttons = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, Qt.Horizontal,
                                         self)
        self._MainLayout.addWidget(self._Buttons)

    def _MakeMainLayout(self):
        self._MainLayout = QVBoxLayout()
        self._MakeButtons()
        self.setLayout(self._MainLayout)

    def _SetSignal(self):
        self._Buttons.accepted.connect(self.accept)
        self._Buttons.rejected.connect(self.reject)


class CreateEnvironmentDialog(AbstractLifeGameDialog):

    def __init__(self, Parent=None):
        super().__init__(Parent)
        self._FixWindowSize(240, 100)
        self.setWindowTitle("Create environment")

    def _MakeSizeLineEdit(self):
        self._SizeLineEdit = QLineEdit(self)
        self._SizeLineEdit.setPlaceholderText("100")
        Validator = QRegExpValidator(QRegExp("[1-9][0-9]*$"), self)
        self._SizeLineEdit.setValidator(Validator)
        self._FormLayout.addRow("Size:", self._SizeLineEdit)

    def _MakeFormLayout(self):
        self._FormLayout = QFormLayout()
        self._MakeSizeLineEdit()
        self._MainLayout.addLayout(self._FormLayout)

    def _MakeMainLayout(self):
        self._MainLayout = QVBoxLayout()
        self._MakeFormLayout()
        self._MakeGPUCheckBox()
        self._MakeButtons()
        self.setLayout(self._MainLayout)

    def _GetSize(self):

        if not self._SizeLineEdit.text():
            return 100

        return int(self._SizeLineEdit.text())

    @staticmethod
    def CreateEnvironment(Parent=None):
        Dialog = CreateEnvironmentDialog(Parent)
        Result = Dialog.exec_()
        Size = Dialog._GetSize()

        return (Size, Dialog._GPUCheckBox.isChecked(), Result == QDialog.Accepted)


class LoadEnvironmentDialog(AbstractLifeGameDialog):

    def __init__(self, Parent=None):
        super().__init__(Parent)
        self._FixWindowSize(300, 100)
        self.setWindowTitle("Load environment")

    def _MakeFilePathLineEdit(self):
        self._FilePathLineEdit = QLineEdit(self)
        Validator = QRegExpValidator(QRegExp('[^\\|/|:|\?|"|<|>|\|\*|\-|\+| |~|\$|\||@|&|]+'),
                                     self)
        self._FilePathLineEdit.setValidator(Validator)
        self._FilePathLayout.addWidget(self._FilePathLineEdit)

    def _MakeFilePathButon(self):
        self._FilePathButton = QToolButton(self)
        self._FilePathLayout.addWidget(self._FilePathButton)

    def _OpenFilePathDialog(self):
        FilePath, _ = QFileDialog.getOpenFileName(
            self, "Please choose load file name", filter="*.npy")
        if not FilePath:
            return

        self._FilePathLineEdit.setText(FilePath)

    def _MakeFilePathLayout(self):
        self._FilePathLayout = QHBoxLayout()
        self._MakeFilePathLineEdit()
        self._MakeFilePathButon()
        self._MainLayout.addLayout(self._FilePathLayout)

    def _SetSignal(self):
        super()._SetSignal()
        self._FilePathButton.clicked.connect(self._OpenFilePathDialog)

    def _MakeMainLayout(self):
        self._MainLayout = QVBoxLayout()
        self._MakeFilePathLayout()
        self._MakeGPUCheckBox()
        self._MakeButtons()
        self.setLayout(self._MainLayout)

    @staticmethod
    def LoadEnvironment(Parent=None):
        Dialog = LoadEnvironmentDialog(Parent)
        Result = Dialog.exec_()
        FilePath = Dialog._FilePathLineEdit.text()

        return (FilePath, Dialog._GPUCheckBox.isChecked(), Result == QDialog.Accepted)
