'''
Copyright (c) 2021 abacus
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses/.
'''

import os
import sys
import random

import LifeGame
import LifeGameDialog
import LifeGameWidget

from PIL import Image
import numpy as np
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *


class LifeGameApplication(QMainWindow):

    def __init__(self):
        super().__init__()
        self.setFont(QFont("Arial"))
        self.LabelFont = QFont("Arial", weight=QFont.Bold, pointSize=15)
        self.setWindowTitle("Game of Life")
        self.IconSize = QSize(30, 30)
        self.IconDirectory = os.path.dirname(
            os.path.abspath(__file__)) + "/Icon/"
        self.setWindowIcon(QIcon(self.IconDirectory + "Icon.ico"))
        self.setWindowFlags(
            Qt.Window | Qt.WindowMinimizeButtonHint | Qt.WindowCloseButtonHint)

        self.MakeTimer()
        self.MakeToolBar()
        self.MakeMainWidget()
        self.show()
        self.SetWindowCenter()
        self.FixWindowSize()
        self.SetSignal()
        self.InitGUI()

    def MakeCreateEnvironmentAction(self):
        self.CreateEnvironmentAction = QAction(QIcon(self.IconDirectory + "CreateEnvironment.svg"),
                                               "Create environment", self)
        self.ToolBar.addAction(self.CreateEnvironmentAction)

    def MakeLoadEnvironmentAction(self):
        self.LoadEnvironmentAction = QAction(QIcon(self.IconDirectory + "LoadEnvironment.svg"), "Load environment",
                                             self)
        self.ToolBar.addAction(self.LoadEnvironmentAction)

    def MakeSaveEnvironmentAction(self):
        self.SaveEnvironmentAction = QAction(QIcon(self.IconDirectory + "SaveEnvironment.svg"), "Save environment",
                                             self)
        self.ToolBar.addAction(self.SaveEnvironmentAction)

    def MakeSaveSnapshotAction(self):
        self.SaveSnapshotAction = QAction(QIcon(self.IconDirectory + "SaveSnapshot.svg"), "Save snapshot",
                                          self)
        self.ToolBar.addAction(self.SaveSnapshotAction)

    def MakeZoomInAction(self):
        self.ZoomInAction = QAction(
            QIcon(self.IconDirectory + "ZoomIn.svg"), "Zoom in", self)
        self.ToolBar.addAction(self.ZoomInAction)

    def MakeZoomOutAction(self):
        self.ZoomOutAction = QAction(
            QIcon(self.IconDirectory + "ZoomOut.svg"), "Zoom out", self)
        self.ToolBar.addAction(self.ZoomOutAction)

    def MakeResetZoomAction(self):
        self.ResetZoomAction = QAction(
            QIcon(self.IconDirectory + "ResetZoom.svg"), "Reset zoom", self)
        self.ToolBar.addAction(self.ResetZoomAction)

    def MakeToolBar(self):
        self.ToolBar = self.addToolBar("ToolBar")
        self.ToolBar.setIconSize(self.IconSize)
        self.ToolBar.setMovable(False)
        self.ToolBar.setContextMenuPolicy(Qt.PreventContextMenu)

        self.MakeCreateEnvironmentAction()
        self.MakeLoadEnvironmentAction()
        self.MakeSaveEnvironmentAction()
        self.MakeSaveSnapshotAction()
        self.MakeZoomInAction()
        self.MakeZoomOutAction()
        self.MakeResetZoomAction()

    def MakeGenerationLabel(self):
        self.GenerationLabel = QLabel(self)
        self.GenerationLabel.setFont(self.LabelFont)
        self.LabelLayout.addWidget(self.GenerationLabel)

    def MakeImageWidget(self):
        self.ImageWidget = LifeGameWidget.ImageWidget(self)
        self.ImageLayout.addWidget(self.ImageWidget, alignment=Qt.AlignCenter)

    def MakeImageGroup(self):
        self.LabelLayout = QHBoxLayout()
        self.ImageLayout = QVBoxLayout()
        self.ImageGroup = QGroupBox()
        self.ImageLayout.addLayout(self.LabelLayout)

        self.MakeGenerationLabel()
        self.MakeImageWidget()

        self.ImageGroup.setLayout(self.ImageLayout)
        self.MainLayout.addWidget(self.ImageGroup)

    def MakeTimer(self):
        self.Timer = QTimer(self)

    def MakeStartStopButton(self):
        self.StartStopButton = QPushButton(self)
        self.StartStopButton.setIconSize(self.IconSize)
        self.ControlLayout.addWidget(self.StartStopButton)

    def MakePreviousButton(self):
        self.PreviousButton = QPushButton(self)
        self.PreviousButton.setToolTip("Previous")
        self.PreviousButton.setIconSize(self.IconSize)
        self.PreviousButton.setIcon(QIcon(self.IconDirectory + "Previous.svg"))
        self.ControlLayout.addWidget(self.PreviousButton)

    def MakeQuitButton(self):
        self.QuitButton = QPushButton(self)
        self.QuitButton.setToolTip("Quit")
        self.QuitButton.setIconSize(self.IconSize)
        self.QuitButton.setIcon(QIcon(self.IconDirectory + "QuitLifeGame.svg"))
        self.ControlLayout.addWidget(self.QuitButton)

    def MakeNextButton(self):
        self.NextButton = QPushButton(self)
        self.NextButton.setToolTip("Next")
        self.NextButton.setIconSize(self.IconSize)
        self.NextButton.setIcon(QIcon(self.IconDirectory + "Next.svg"))
        self.ControlLayout.addWidget(self.NextButton)

    def MakeSpeedLabel(self):

        self.SpeedLayout.addWidget(QLabel("Slow", self), 0, 0)
        self.SpeedLayout.addWidget(QLabel("Fast", self), 0, 2)

        self.SpeedLabel = QLabel(self)
        self.SpeedLayout.addWidget(
            self.SpeedLabel, 1, 1, alignment=Qt.AlignCenter)

    def MakeSpeedSlider(self):
        self.SpeedSlider = QSlider(Qt.Horizontal, self)
        self.SpeedSlider.setToolTip("Change speed")
        self.SpeedSlider.setMaximum(50)
        self.SpeedSlider.setMinimum(1)
        self.SpeedSlider.setPageStep(5)
        self.SpeedLayout.addWidget(self.SpeedSlider, 0, 1)

    def MakeSpeedGroup(self):
        self.SpeedLayout = QGridLayout()
        self.SpeedGroup = QGroupBox()
        self.SpeedGroup.setAlignment(Qt.AlignCenter)
        self.SpeedGroup.setTitle("Simulation speed")

        self.MakeSpeedLabel()
        self.MakeSpeedSlider()

        self.SpeedGroup.setLayout(self.SpeedLayout)
        self.ControlLayout.addWidget(self.SpeedGroup)

    def MakeControlGroup(self):
        self.ControlLayout = QHBoxLayout()
        self.ControlGroup = QGroupBox()

        self.MakeStartStopButton()
        self.MakePreviousButton()
        self.MakeQuitButton()
        self.MakeNextButton()
        self.MakeSpeedGroup()

        self.ControlGroup.setLayout(self.ControlLayout)
        self.MainLayout.addWidget(self.ControlGroup)

    def MakeMainWidget(self):
        self.MainLayout = QVBoxLayout()
        self.MainWidget = QWidget(self)

        self.MakeImageGroup()
        self.MakeControlGroup()

        self.MainWidget.setLayout(self.MainLayout)
        self.setCentralWidget(self.MainWidget)

    def SetWindowCenter(self):
        FG = self.frameGeometry()
        Center = QDesktopWidget().availableGeometry().center()
        FG.moveCenter(Center)
        self.move(FG.topLeft())

    def FixWindowSize(self):
        self.setFixedSize(self.size())
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

    def SetSignal(self):
        self.CreateEnvironmentAction.triggered.connect(self.CreateEnvironment)
        self.LoadEnvironmentAction.triggered.connect(self.LoadEnvironment)
        self.SaveEnvironmentAction.triggered.connect(self.SaveEnvironment)
        self.SaveSnapshotAction.triggered.connect(self.SaveSnapshot)
        self.ZoomInAction.triggered.connect(self.ZoomIn)
        self.ZoomOutAction.triggered.connect(self.ZoomOut)
        self.ResetZoomAction.triggered.connect(self.ResetZoom)
        self.Timer.timeout.connect(self.Update)
        self.StartStopButton.clicked.connect(self.ToggleLifeGame)
        self.PreviousButton.clicked.connect(self.Previous)
        self.QuitButton.clicked.connect(self.QuitLifeGame)
        self.NextButton.clicked.connect(self.Next)
        self.SpeedSlider.valueChanged.connect(self.ChangeSpeed)

    def InitGenerationLabel(self):
        self.GenerationLabel.setText("Generation:")

    def UpdateGenerationLabel(self, Generation):
        self.GenerationLabel.setText("Generation:{0}".format(Generation))

    def InitSpeedSlider(self):
        self.SpeedSlider.setValue(20)

    def UpdateSpeedLabel(self):
        self.SpeedLabel.setText(
            "{0} steps/second".format(self.SpeedSlider.value()))

    def InitLabel(self):
        self.InitGenerationLabel()

    def EnableWidgets(self):
        self.SaveEnvironmentAction.setEnabled(True)
        self.SaveSnapshotAction.setEnabled(True)
        self.ZoomInAction.setEnabled(True)
        self.ZoomOutAction.setEnabled(True)
        self.ResetZoomAction.setEnabled(True)
        self.StartStopButton.setEnabled(True)
        self.PreviousButton.setEnabled(True)
        self.QuitButton.setEnabled(True)
        self.NextButton.setEnabled(True)
        self.SpeedSlider.setEnabled(True)

    def DisableWidgets(self):
        self.SaveEnvironmentAction.setEnabled(False)
        self.SaveSnapshotAction.setEnabled(False)
        self.ZoomInAction.setEnabled(False)
        self.ZoomOutAction.setEnabled(False)
        self.ResetZoomAction.setEnabled(False)
        self.StartStopButton.setEnabled(False)
        self.PreviousButton.setEnabled(False)
        self.QuitButton.setEnabled(False)
        self.NextButton.setEnabled(False)
        self.SpeedSlider.setEnabled(False)

    def InitGUI(self):
        self.StopLifeGame()
        self.LifeGame = None
        self.InitLabel()
        self.ImageWidget.InitWidget()
        self.InitSpeedSlider()
        self.DisableWidgets()

    def CreateNewLifeGame(self, Seed=0, GPU=True, Size=100, FilePath=None):
        try:
            LG = LifeGame.LifeGame(
                Seed=Seed, GPU=GPU, Size=Size, FilePath=FilePath)
        except Exception as e:
            QMessageBox.critical(self, "Critical issue",
                                 str(e.args[0]), QMessageBox.Ok)
        else:
            self.InitGUI()
            self.EnableWidgets()
            self.LifeGame = LG
            self.DrawEnvironment(self.LifeGame.GetGeneration(),
                                 self.LifeGame.GetEnvironment())
            self.CheckStack()

    def CreateEnvironment(self):
        self.StopLifeGame()

        Size, GPU, Result = LifeGameDialog.CreateEnvironmentDialog.CreateEnvironment(
            self)
        Seed = random.randint(1, 10000)
        if not Result:
            return

        self.CreateNewLifeGame(Seed=Seed, GPU=GPU, Size=Size)

    def LoadEnvironment(self):
        self.StopLifeGame()

        FilePath, GPU, Result = LifeGameDialog.LoadEnvironmentDialog.LoadEnvironment(
            self)
        if (not Result) or (not FilePath):
            return

        self.CreateNewLifeGame(GPU=GPU, FilePath=FilePath)

    def SaveEnvironment(self):
        self.StopLifeGame()

        FilePath, _ = QFileDialog.getSaveFileName(self, "Please choose save directory and name",
                                                  filter="*.npy")
        if not FilePath:
            return
        self.LifeGame.SaveEnvironmnet(FilePath)

    def QuitLifeGame(self):
        self.StopLifeGame()

        Result = QMessageBox.warning(self, "Warning", "Are you sure you want to quit this simulation?",
                                     QMessageBox.Ok | QMessageBox.Cancel)

        if Result == QMessageBox.Cancel:
            return

        self.InitGUI()

    def StartTimer(self):
        Interval = int(1000 / self.SpeedSlider.value())
        self.Timer.start(Interval)

    def CheckStack(self):
        if self.LifeGame.IsStackEmpty():
            self.PreviousButton.setEnabled(False)
        else:
            self.PreviousButton.setEnabled(True)

    def DrawEnvironment(self, Generation, Environment):
        self.ImageWidget.DrawMatrix(Environment)
        self.UpdateGenerationLabel(Generation)

    def Update(self):
        Generation, Environment = self.LifeGame.Next()
        self.DrawEnvironment(Generation, Environment)
        self.CheckStack()

    def Next(self):
        self.StopLifeGame()
        self.Update()

    def Previous(self):
        self.StopLifeGame()
        Generation, Environment = self.LifeGame.Previous()

        if (Generation is None) and (Environment is None):
            return

        self.DrawEnvironment(Generation, Environment)
        self.CheckStack()

    def StartLifeGame(self):
        self.StartTimer()
        self.StartStopButton.setToolTip("Stop")
        self.StartStopButton.setIcon(
            QIcon(self.IconDirectory + "StopLifeGame.svg"))

    def StopLifeGame(self):
        self.Timer.stop()
        self.StartStopButton.setToolTip("Start")
        self.StartStopButton.setIcon(
            QIcon(self.IconDirectory + "StartLifeGame.svg"))

    def ToggleLifeGame(self):

        if self.Timer.isActive():
            self.StopLifeGame()
        else:
            self.StartLifeGame()

    def ChangeSpeed(self):
        self.UpdateSpeedLabel()

        if not self.Timer.isActive():
            return

        self.StartTimer()

    def QPixmap2Matrix(self, Pixmap):
        Image = Pixmap.toImage()
        Buffer = Image.bits().asarray(Image.width() * Image.height() * 4)
        Matrix = np.ndarray(buffer=Buffer, dtype=np.uint8,
                            shape=(Image.height(), Image.width(), 4))

        return Matrix.copy()

    def SetExtension(self, FilePath, Ext):
        Tmp, _ = os.path.splitext(FilePath)
        FilePath = Tmp + Ext

        return FilePath

    def SaveSnapshot(self):
        self.StopLifeGame()

        Matrix = self.QPixmap2Matrix(self.ImageGroup.grab())
        FilePath, Ext = QFileDialog.getSaveFileName(self, "Please choose save directory and name",
                                                    filter="*.png")
        if not FilePath:
            return

        FilePath = self.SetExtension(FilePath, Ext[1:])

        Img = Image.fromarray(Matrix)
        r, g, b, a = Img.split()
        Img = Image.merge("RGBA", (b, g, r, a))
        Img.save(FilePath, "PNG")

    def ZoomIn(self):
        self.ImageWidget.ZoomIn()

    def ZoomOut(self):
        self.ImageWidget.ZoomOut()

    def ResetZoom(self):
        self.ImageWidget.ResetZoom()

    def Flip(self, x, y):
        self.LifeGame.Flip(y, x)
        self.DrawEnvironment(self.LifeGame.GetGeneration(),
                             self.LifeGame.GetEnvironment())


if __name__ == "__main__":
    App = QApplication(sys.argv)
    MainWindow = LifeGameApplication()
    sys.exit(App.exec())
