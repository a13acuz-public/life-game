'''
Copyright (c) 2021 abacus
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses/.
'''

import os
import argparse

import cupy as cp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


class LifeGame(object):

    def __init__(self, Seed=0, GPU=True, Size=100, FilePath=None):
        assert type(Seed) == int, "'Seed' must be integer"
        assert type(GPU) == bool, "'GPU' must be boolean"
        assert type(Size) == int, "'Size' must be integer"
        assert Size >= 3, "'Size' must be larger than 2"
        assert Size < 2000, "'Size' must be smaller than 2000"

        np.random.seed(seed=Seed)
        cp.random.seed(seed=Seed)

        self._GPU = GPU
        self._Generation = 1
        self._Stack = []

        self.xp = cp if self._GPU else np

        if FilePath is not None:
            self._Size, self._Environment = self._LoadEnvironment(FilePath)

        else:
            self._Size = Size
            self._InitEnvironment()

    def _InitEnvironment(self):

        self._Environment = self.xp.random.randint(
            0, 2, (self._Size, self._Size))

    def GetEnvironment(self):
        return (self._Environment.get() if self._GPU else self._Environment.copy())

    def GetGeneration(self):
        return self._Generation

    def _LoadEnvironment(self, FilePath):
        assert os.path.isfile(FilePath), "Invalid file path"
        _, Ext = os.path.splitext(FilePath)
        assert Ext == ".npy", "Extension must be '.npy'"

        try:
            Environment = np.load(FilePath)
        except Exception as e:

            raise e

        assert len(Environment.shape) == 2, "Environment must be 2D array"
        assert Environment.shape[0] == Environment.shape[1], "Environment must be square matrix"

        Size = Environment.shape[0]
        assert Size >= 3, "Size of environment must be larger than 2"
        assert Size < 2000, "Size of environment must be smaller than 2000"
        assert (Environment.dtype == np.int) or (
            Environment.dtype == np.float), "'dtype' of environment must be 'int' or 'float'"
        assert (0 <= Environment.min()) and (
            Environment.max() <= 1), "All values in the environment must be in the range of 0.0 and 1.0"

        return Size, cp.array(Environment) if self._GPU else Environment.copy()

    def SaveEnvironmnet(self, FilePath):
        Directory = os.path.dirname(FilePath)
        assert os.path.isdir(Directory), "Invalid directory"

        np.save(FilePath, self.GetEnvironment())

    def _MakeFigure(self):
        fig = plt.figure(figsize=(8, 8))
        ax = fig.add_subplot(1, 1, 1)

        return fig, ax

    def _DrawHeatMap(self, ax):
        ax.pcolor(self.GetEnvironment(), cmap=plt.cm.Greens)
        ax.tick_params(axis="both", which="both", bottom="off",
                       left="off", labelbottom="off", labelleft="off")
        ax.set_title("Generation {0}".format(self._Generation), fontsize=20)

        return ax

    def Draw(self):
        fig, ax = self._MakeFigure()
        self._DrawHeatMap(ax)

        fig.show()

    def DrawAnimation(self):
        fig, ax = self._MakeFigure()

        def update(i):
            ax.clear()
            self.Next()
            self._DrawHeatMap(ax)

        Animation = animation.FuncAnimation(fig, update, interval=50)
        plt.show()

    def IsStackEmpty(self):
        if not self._Stack:
            return True
        else:
            return False

    def _PushEnvironment(self, Environment):

        if len(self._Stack) == 100:
            self._Stack.pop(0)

        self._Stack.append(Environment)

    def Previous(self):

        if not self._Stack:
            return None, None

        self._Environment = self._Stack.pop()

        self._Generation -= 1

        return self.GetGeneration(), self.GetEnvironment()

    def Flip(self, x, y):
        assert (0 <= x) and (x < self._Size) and (
            0 <= y) and (y < self._Size), "Index out of range"

        self._Environment[x][y] = 1 - self._Environment[x][y]

    def Next(self):

        self._PushEnvironment(self._Environment)

        Values = self.xp.zeros((self._Size, self._Size))

        Values += self._SlideDown(self._Environment)

        Values += self._SlideUp(self._Environment)

        Values += self._SlideLeft(self._Environment)

        Values += self._SlideRight(self._Environment)

        Values += self._SlideLowerRight(self._Environment)

        Values += self._SlideLowerLeft(self._Environment)

        Values += self._SlideUpperRight(self._Environment)

        Values += self._SlideUpperLeft(self._Environment)

        self._Environment = self._UpdateEnvironment(Values)

        self._Generation += 1

        return self.GetGeneration(), self.GetEnvironment()

    def _UpdateEnvironment(self, Values):

        NewEnvironment = self.xp.zeros((self._Size, self._Size))

        NewEnvironment += self.xp.where(((Values == 2) |
                                        (Values == 3)) & (self._Environment == 1), 1, 0)

        NewEnvironment += self.xp.where((Values == 3)
                                        & (self._Environment == 0), 1, 0)

        return NewEnvironment.copy()

    def _SlideUp(self, Matrix):
        return self.xp.concatenate((Matrix[1:, :], Matrix[0:1, :]), axis=0)

    def _SlideDown(self, Matrix):
        return self.xp.concatenate((Matrix[-1:, :], Matrix[:-1, :]), axis=0)

    def _SlideLeft(self, Matrix):
        return self.xp.concatenate((Matrix[:, 1:], Matrix[:, 0:1]), axis=1)

    def _SlideRight(self, Matrix):
        return self.xp.concatenate((Matrix[:, -1:], Matrix[:, :-1]), axis=1)

    def _SlideUpperLeft(self, Matrix):
        return self._SlideLeft(self._SlideUp(Matrix))

    def _SlideUpperRight(self, Matrix):
        return self._SlideRight(self._SlideUp(Matrix))

    def _SlideLowerLeft(self, Matrix):
        return self._SlideLeft(self._SlideDown(Matrix))

    def _SlideLowerRight(self, Matrix):
        return self._SlideRight(self._SlideDown(Matrix))


def ParseArguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-sd", "--seed", type=int, default=0)
    parser.add_argument("-g", "--gpu", action="store_true", default=True)
    parser.add_argument("-sz", "--size", type=int, default=100)
    return parser


if __name__ == "__main__":

    parser = ParseArguments()
    args = parser.parse_args()

    LG = LifeGame(Seed=args.seed, GPU=args.gpu, Size=args.size)
    LG.DrawAnimation()
